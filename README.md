# multdiv-simulator



## Introduction

Compute signed integer multiplication and division via multiple algorithms, 
printing colorized progress along the way. 

For multiplication, red is used to indicate bits that are part of the multiplier being consumed. For division, green is used to indicate bits of the completed quotient as they are created.

Created by Tyler Bletsch for Duke ECE 350.

**ACADEMIC INTEGRITY WARNING**: Using this tool to do your homework for you is a violation of the academic integrity policy. Learn from the tool, don't let it replace you!

## Usage

Just download or clone both py files into a directory, then run with python 3. 

Usage documentation available with `-h`:

```
$ ./multdivsim.py -h
usage: multdivsim.py [-h] [-b BITS] -a {mult-naive,mult-booth,mult-mbooth,mult-mbooth-bb,div-restore,div-nonrestore} v1 v2

Compute signed integer multiplication and division via multiple algorithms, printing colorized progress along the way. By Tyler Bletsch for Duke ECE 350.

positional arguments:
  v1                    First operand (multiplicand/dividend)
  v2                    Second operand (multiplier/divisor)

optional arguments:
  -h, --help            show this help message and exit
  -b BITS               An integer value for BITS, defaults to 'just big enough' (default: None)
  -a {mult-naive,mult-booth,mult-mbooth,mult-mbooth-bb,div-restore,div-nonrestore}
                        Choose the operation and algorithm to do (default: None)

Supported algorithms for -a:
  mult-naive     : Naive signed multiplication
  mult-booth     : Classic Booth's signed multiplication
  mult-mbooth    : Modified Booth's signed multiplication (no 'bonus bit', has edge case errors, e.g. 6x6 4-bit)
  mult-mbooth-bb : Modified Booth's signed multiplication with a 'bonus bit'
  div-restore    : Restore-method signed division
  div-nonrestore : Non-restore-method signed division
```

## Usage examples

Note: actual terminal output will show color not present in the examples below.

Naive 5x3:

```
$ ./multdivsim.py -a mult-naive 5 3
BITS: 4
Algorithm: mult-naive
Operand 1: 5
Operand 2: 3

  Reg4(0101) = 5
x Reg4(0011) = 3

Init           00000011
Add? yes       01010011
Log Shift      00101001
Add? yes       01111001
Log Shift      00111100
Add? no        00111100
Log Shift      00011110
Add? no        00011110
Log Shift      00001111
               = 15
```

Naive 5x3, but forced to use 6 bits:
```
$ ./multdivsim.py -a mult-naive -b 6 5 3
BITS: 6
Algorithm: mult-naive
Operand 1: 5
Operand 2: 3

  Reg6(000101) = 5
x Reg6(000011) = 3

Init           000000000011
Add? yes       000101000011
Log Shift      000010100001
Add? yes       000111100001
Log Shift      000011110000
Add? no        000011110000
Log Shift      000001111000
Add? no        000001111000
Log Shift      000000111100
Add? no        000000111100
Log Shift      000000011110
Add? no        000000011110
Log Shift      000000001111
               = 15
```

Classic Booth's algorithm 5x3:

```
$ ./multdivsim.py -a mult-booth 5 3
BITS: 4
Algorithm: mult-booth
Operand 1: 5
Operand 2: 3

  Reg4(0101) = 5
x Reg4(0011) = 3

Init           00000011 0
Add/sub? sub   10110011 0
Shift          11011001 1
Add/sub? no    11011001 1
Shift          11101100 1
Add/sub? add   00111100 1
Shift          00011110 0
Add/sub? no    00011110 0
Shift          00001111 0
               = 15
```

Modified Booth's algorithm, but *without* adding an extra bit to prevent overflows in corner cases involving +2m/-2m:
```
$ ./multdivsim.py -a mult-mbooth 5 3
BITS: 4
Algorithm: mult-mbooth
Operand 1: 5
Operand 2: 3

  Reg4(0101) = 5
x Reg4(0011) = 3

Init           00000011 0
Add/sub? -m    10110011 0
Shift          11101100 1
Add/sub? +m    00111100 1
Shift          00001111 0
               = 15
```

Modified Booth's algorithm, but with a "bonus bit" (1 bit larger product register) to fix the +2m/-2m corner case bug, 5x3:

```
$ ./multdivsim.py -a mult-mbooth-bb 5 3
BITS: 4
Algorithm: mult-mbooth-bb
Operand 1: 5
Operand 2: 3

  Reg4(0101) = 5
x Reg4(0011) = 3

Init           000000011 0
Add/sub? -m    110110011 0
Shift          111101100 1
Add/sub? +m    000111100 1
Shift          000001111 0
Truncate        00001111
               = 15
```

Restore method division, 7/2:

```
$ ./multdivsim.py -a div-restore 7 2
BITS: 4
Algorithm: div-restore
Operand 1: 7
Operand 2: 2

  Reg4(0111) = 7
/ Reg4(0010) = 2

Init             00000111
n=4
LShift RQ        00001110
R -= V           11101110
Q[0]=0, restore  00001110
n=3
LShift RQ        00011100
R -= V           11111100
Q[0]=0, restore  00011100
n=2
LShift RQ        00111000
R -= V           00011000
Q[0]=1           00011001
n=1
LShift RQ        00110010
R -= V           00010010
Q[0]=1           00010011
                 = 3 R 1
```

Non-restore method division, 7/2:
```
$ ./multdivsim.py -a div-nonrestore 7 2
BITS: 4
Algorithm: div-nonrestore
Operand 1: 7
Operand 2: 2

  Reg4(0111) = 7
/ Reg4(0010) = 2

Init             00000111
n=4
LShift RQ        00001110
R -= V           11101110
Q[0]=0           11101110
n=3
LShift RQ        11011100
R += V           11111100
Q[0]=0           11111100
n=2
LShift RQ        11111000
R += V           00011000
Q[0]=1           00011001
n=1
LShift RQ        00110010
R -= V           00010010
Q[0]=1           00010011
Q[0]=1           00010011
                 = 3 R 1
```

